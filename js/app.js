// When the user scrolls the page, execute myFunction
window.onscroll = function() {myFunction()};

// Get the header
var texto = document.getElementById("speach");

// Get the offset position of the navbar
var sticky = texto.offsetTop;

// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
  if (window.pageYOffset > sticky) {
    texto.classList.add("sticky");
  } else {
    texto.classList.remove("sticky");
  }
}